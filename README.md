# Intel Graphics Documentation

Content is listed below. Currently, it's only the PRMs, but maybe this repo will
contain more docs in the future.

### Programmer Reference Manuals for Intel graphics hardware

See `prm/`.
It is a mirror of the PDFs at Intel's official site,
as well as PDFs of older GPU architectures no longer hosted there.
This mirror provides bulk download of the PDFs via git,
and better organization than Intel's site.

### Layout

- `prm/`
    - `index.html`      -- Descriptive list of all PRMs, with full PDF titles.
    - `<codename>/`     -- PRMs for GPU *codename*.
    - `by-gen/`         -- Symlinks to *codename* dirs, sorted by hardware generation.
  
### TODO

- Add a page  for easy lookup of *(PCI ID -> codename -> hw config)*,
  preferably generated from Mesa source.
